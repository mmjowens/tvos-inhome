var expect = require('chai').expect,
    request = require('request-promise'),
    network = require('../lib/network');

describe('Home end point', () => {
    var url = 'http://localhost:9001/';
    it('returns status 200', () => {
        return request(url, (err, response, body) => {
            expect(response.statusCode).to.equal(200);

        });
    });
});

describe('MenuBar end point', () => {
    var url = 'http://localhost:9001/tvml/MenuBar.xml.js';
    it('returns status 200', () => {
        return request(url, (err, response, body) => {
            expect(response.statusCode).to.equal(200);

        });
    });
});



