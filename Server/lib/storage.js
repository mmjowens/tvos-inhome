var store={};


/**
 * Exported Functions
 */
exports.get = getData;
exports.set = saveData;


/**
 * Save a value to our temp store
 * @param key
 * @param value
 */
function saveData(key, value){
	store[key] = value;
}

/**
 * Get a value to our temp store
 * @param key
 */
function getData(key){
	return store[key]||false;
}