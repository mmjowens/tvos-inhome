var assert = require('chai').assert,
    network = require('../lib/network');

describe("Get CatalogMenu", () => {
    it('returns catalogMenu', () => {
        return network.getCatalogMenu().then(x => {})
    });
    it('catalogMenu.result.label is Catalog', () => {
        return network.getCatalogMenu().then(x => {
            assert.equal(x.result.label, 'Catalog')
        })
    });
    it('catalogMenu is an object', () => {
        return network.getCatalogMenu().then(x => {
            assert.isObject(x);
        })
    })
});


