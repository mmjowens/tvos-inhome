## Getting Started

1. cd AppleTV
2. open ProtoView.xcodeproj/
3. cd .. 
4. cd Server
5. npm install
6. npm start
7. Choose Apple tv Sim.
8. Run project inside Xcode

## Bearer Token
Bearer token will need to be updated when using the app for the first time or for the first time each day.
Navigate to Server/routers/tvml.js, change 'token' variable on line 5.