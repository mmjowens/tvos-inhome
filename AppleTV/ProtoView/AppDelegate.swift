//
//  AppDelegate.swift
//  RWDevCon
//
//  Created by Kelvin on 2015-09-14.
//  Copyright © 2015 RayWenderlich. All rights reserved.
//

import TVMLKit
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, TVApplicationControllerDelegate {
    
    var window: UIWindow?
    var appController: TVApplicationController?
    
    static let TVBaseURL = "http://localhost:9001/"
    
    static let TVBootURL = "\(AppDelegate.TVBaseURL)js/application.js"
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let appControllerContext = TVApplicationControllerContext()
        
        guard let javaScriptURL = URL(string: AppDelegate.TVBootURL) else { fatalError("unable to create NSURL") }
        
        appControllerContext.javaScriptApplicationURL = javaScriptURL
        
        appControllerContext.launchOptions["BASEURL"] = AppDelegate.TVBaseURL
        
    
        appController = TVApplicationController(context: appControllerContext, window: window, delegate: self)
        
        return true
    }
    
    func appController(_ appController: TVApplicationController, didFail error: Error) {
        print("\(#function) invoked with error: \(error)")
        
        let title = "Error Launching Application"
        let message = error.localizedDescription
        let alertController = UIAlertController(title: title, message: message, preferredStyle:.alert)
        
        self.appController?.navigationController.present(alertController, animated: true, completion: nil)
    }

    
}

