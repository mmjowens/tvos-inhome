var request = require('request-promise'),
    db = require('../lib/storage'),
    fs = require('fs'),
    token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJrZXkiOiJqb3dlbnNAbWFkbW9iaWxlLmNvbSIsInRlbmFudCI6Im1hY3lzLWRlbW8iLCJ0eXBlIjoiaW5zdGFuY2UiLCJsZXZlbCI6Im1hbmFnZW1lbnQiLCJhdXRoVHlwZSI6InR3b2ZhY3RvciIsInBlcm1pc3Npb25zIjpbImFsbCAqIl0sImlhdCI6MTQ3OTgyMDE2NiwiZXhwIjoxNDc5OTA2NTY2LCJhdWQiOiJtYWN5cy1kZW1vIn0.aPuDkIUjua2HeSwtOVtw5MYLBy5n7Eee3C-58TjtKp9oqpQzhrBwJGhAWAkRbhJQTAc3GyneqNyehhWkEjf0kUTgSfM1jRiWxtHH0M04iQo4az24WmSRAn5y-QumQ54F2CvhLWOIV-2zME7zkWCDyRTYTlFJ7OavdLZCGambM3oZF1PbNXSy5i77zbIfom2zPnj8_KOcRme1_aij2elIhpk_k7saGKwLV9YiSb6bSajJb0Q1LyyDb9VKZO23e77bTFkJjzoL326JJAnYyQkH4yT_g-qxokdlMeiNKjn22knCppxcLyVga8azWxg4PQfcTqrf3gKlAG_Bh7RA1tzEcg";


const headers = {
    "Content-Type": "application/json",
    "Authorization": "bearer " + token
};

const catalogMenuEndPoint = {
    url: "https://macys-demo.madcloud.io/api/v1/concierge/apparel/getcatalogmenu",
    method: 'GET',
    headers: headers,
    json: true
};

module.exports.getCatalogMenu = () => {
    return new Promise((resolve, reject) => {
        request(catalogMenuEndPoint).then(
            data => {
                resolve(data)
            }
        ).catch(err => {
            console.log(err);
        })
    })

};

module.exports.getProductList = (categoryMenu, view, res) => {
    let catalogMenu = db.get('catalogMenu');
    let productListPath = [];
    categoryMenu.children.forEach(x => {
        productListPath.push(x.path)
    });
    let requests = productListPath.map(path => {
        let productListEndPoint = {
            url: "https://macys-demo.madcloud.io/api/v1/concierge/apparel/getproductlist?products_url=" + path,
            method: 'GET',
            headers: headers,
            json: true
        };
        return new Promise((resolve, reject) => {
            request(productListEndPoint).then(
                data => {
                    resolve(data)
                },
                err => {
                    resolve()
                }
            )
        })
    });
    Promise.all(requests)
        .then(data => {
            let productList = data.filter(x => {
                if (x !== undefined) return x
            });
            db.set('productList', productList);
            res.render(view, {
                productList: productList
            });
        }).catch(err => {
        console.log(err);

    });
};

module.exports.getProductDescription = (prodID, view, res) => {
    let productDescriptionEndPoint = {
        url: 'https://macys-demo.madcloud.io/api/v1/concierge/apparel/getproductdetail?product_url=' + prodID,
        method: 'GET',
        headers: headers,
        json: true
    };
    let recommendedEndPoint = {
        url: 'https://macys-demo.madcloud.io/api/v1/concierge/apparel/getrecommendedproducts?productId=' + prodID,
        method: 'GET',
        headers: headers,
        json: true
    };
    request(productDescriptionEndPoint)
        .then(data => {
            db.set('productDescription', data);
            return request(recommendedEndPoint)
        })
        .then(data => {
            db.set('recommendedProducts', data);
            res.render(view, {
                productDescription: db.get('productDescription'),
                recommendedProducts: db.get('recommendedProducts')
            })

        })
        .catch(err => {
            console.log(err.statusCode);
            res.render('tvml/Alert');
        })
};


module.exports.getQRCode = prodID => {
    let URL = 'http://localhost:9001/tvml/QRCode.js?id='

    let buildURL = (element, index, array) => {
        if (index === 0) {
            URL += element.product.id
        } else {
            URL += '%20' + element.product.id
        }
    };

    prodID.forEach(buildURL);

    return URL;


};

