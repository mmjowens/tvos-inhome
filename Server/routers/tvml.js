var express = require('express'),
    router = express.Router(),
    db = require('../lib/storage'),
    network = require('../lib/network'),
    qr = require('qr-image'),
    fs = require('fs'),
    lookHolder = [];

/**
 * Our TVML Routes
 *
 */


router.use((req, res, next) => {
    console.log("Request--> " + req.originalUrl);
    next();
});


/**
 * Alert Views Block start
 */
router.get('/tvml/Alert.xml.js', (req, res) => {
    res.render('tvml/Alert');
});

router.get('/tvml/AlertWithDescription.xml.js', (req, res) => {
    res.render('tvml/AlertWithDescription', {productDescription: db.get('productDescription')});
});
/**
 * Alert View block end
 */

/**
 * Catalog MenuBar and View block start
 */
router.get('/tvml/CatalogMenuBar.xml.js', (req, res) => {
    try {
        network.getCatalogMenu().then(data => {
            db.set('catalogMenu', data)
            lookHolder = [];
            res.render('tvml/CatalogMenuBar', {catalogMenu: data})
        })
    } catch (err) {
        console.log(err)
        res.render('tvml/Alert');
    }
});

router.get('/tvml/Catalog.xml.js', (req, res) => {
    res.render('tvml/Catalog', {catalogMenu: db.get('catalogMenu')})
});
/**
 * Catalog MenuBar and View block end
 */

/**
 * Category MenuBar and View block start
 */
router.get('/tvml/CategoryMenuBar.xml.js', (req, res) => {
    if (req.query.id) {
        let id = [] = req.query.id.split(',');
        res.render('tvml/CategoryMenuBar', {id: id})
    } else {
        res.render('tvml/Alert');
    }


});

router.get('/tvml/Category.xml.js', (req, res) => {
    if (req.query.id) {
        let id = [] = req.query.id.split(',');
        if (db.get('catlogMenu')) {
            let catalogMenu = db.get('catalogMenu');
            db.set('categoryMenu', catalogMenu.result[id[0]].children[id[1]])
            let categoryMenu = db.get('categoryMenu');
            network.getProductList(categoryMenu, 'tvml/Category', res)
        } else {
            network.getCatalogMenu().then(data => {
                db.set('catalogMenu', data)
                let catalogMenu = db.get('catalogMenu');
                db.set('categoryMenu', catalogMenu.result[id[0]].children[id[1]])
                let categoryMenu = db.get('categoryMenu');
                network.getProductList(categoryMenu, 'tvml/Category', res)
            })
        }
    } else {
        res.render('tvml/Alert');
    }
});
/**
 * Category MenuBar and View block end
 */

/**
 * Product MenuBar and View block start
 */
router.get('/tvml/ProductMenuBar.xml.js', (req, res) => {
    if (req.query.id) {
        let id = [] = req.query.id

        res.render('tvml/ProductMenuBar', {pid: id, lookHolder: lookHolder})
    } else {
        res.render('tvml/Alert');
    }

});

router.get('/tvml/Product.xml.js', (req, res) => {
    if (req.query.id) {
        let prodID = req.query.id;
        network.getProductDescription(prodID, 'tvml/Product', res)
    } else {
        res.render('tvml/Alert');
    }
});
/**
 * Product MenuBar and View block end
 */

/**
 * TODO: Build New MenuBar approach
 */
router.get('/tvml/RootMenuBar.xml.js', (req, res) => {
    if (req.query.id) {
        let id = [] = req.query.id.split(',');
        res.render('tvml/CategoryMenuBar', {id: id})
    } else if (req.query.pid) {
        let pid = [] = req.query.pid
        console.log('lookholder.length : ', lookHolder.length);
        res.render('tvml/ProductMenuBar', {pid: pid,  lookHolder: lookHolder})
    } else if (db.get('catalogMenu')) {
        lookHolder = []
        res.render('tvml/CatalogMenuBar');
    } else {
        try {
            network.getCatalogMenu().then(data => {
                lookHolder = [];
                db.set('catalogMenu', data)
                res.render('tvml/CatalogMenuBar')
            })
        } catch (err) {
            console.log(err)
            res.render('tvml/Alert');
        }
    }
});


router.get('/tvml/SingleProductLook.xml.js', (req, res) => {
    let productDescription = db.get('productDescription');
    let product = {
        product: {
            id: productDescription.result.id,
            image: productDescription.result.images[0].source
        }
    };
    lookHolder.push(product);
    res.render('tvml/SingleProductLook', {productDescription: db.get('productDescription'), lookHolder: lookHolder})
});

router.get('/tvml/LookStreams.xml.js', (req, res) => {
    let productList = [] = db.get('productList');
    let filterTargets = value => {
        switch (value.result.name) {
            case 'Jeans':
                return value;
                break;
            case 'Pants & Capris':
                return value;
                break;
            case 'Skirts':
                return value;
                break;
            case 'Tops':
                return value;
                break;
            case 'Sweaters':
                return value;
                break;
            case 'Coats':
                return value;
                break;
            case 'Jackets':
                return value;
                break;
            default:
                return;
        }
    };
    let suggestedProductList = [] = productList.filter(filterTargets);
    res.render('tvml/LookStreams', {suggestedProductList: suggestedProductList})
});

router.get('/tvml/CompletedLook.xml.js', (req, res) => {
    res.render('tvml/CompletedLook', {lookHolder: lookHolder});
});

router.get('/tvml/LookQR.xml.js', (req, res) => {

    let url = network.getQRCode(lookHolder)
    console.log('url : ', url)
    console.log('lookholder : ', lookHolder);
    res.render('tvml/LookQR', {qrCode: url, lookHolder: lookHolder});
});

router.get('/tvml/QRCode.js', (req, res) => {
    try {
        let img = qr.image(req.query.id);
        res.writeHead(200, {'Content-type': 'image/png'});
        img.pipe(res);
    } catch (e) {
        //res.writeHead(414, { 'Content-Type' : 'text/html'});
        res.render('tvml/Alert');
    }
});
/**
 * And Our Default Route
 */
router.get('/', (req, res) => {
    try {
        network.getCatalogMenu().then(data => {
            db.set('catalogMenu', data)
            res.render('tvml/CatalogMenuBar')
        })
    } catch (err) {
        console.log(err)
        res.render('tvml/Alert');
    }
    // record the application base URL here
    db.set("baseURL", req.protocol + '://' + req.get('host'));
});

// export
module.exports = router