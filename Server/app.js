var express = require('express'),
    app = express();
var http = require('http').Server(app);

// set the view engine to ejs
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');


// set up our static file route
app.use(express.static(__dirname + '/public'));

// set up our routes
app.use(require('./routers/tvml'));

// Start our Server
http.listen(9001, function () {
    console.log('Node tvOS sample app now listening on port: %s', http.address().port);
});
